<?php
require 'mysqlConnect.php';
// Required field names
$required = array('username', 'email', 'age', 'description');

// Loop over field names, make sure each one exists and is not empty
$error = false;
foreach($required as $field) {
  if (empty($_POST[$field])) {
    $error = true;
  }
}

if ($error) {
  echo "All fields are required.";
  exit;
}

$username = $_POST["username"];
$email = $_POST["email"];
$age = (int)$_POST["age"];
$description = $_POST["description"];

// Get the filename and make sure it is valid
$filename = basename($_FILES['uploadedfile']['name']);
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	echo "Invalid filename";
	exit;
}
 
// Get the username and make sure it is valid
if( !preg_match('/^[\w_\-]+$/', $username) ){
	echo "Invalid username";
	exit;
}

$target_dir = "uploads/";
$url = $target_dir . $filename;

echo $url;

if (move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $url)) {
        echo "The file ". $filename . " has been uploaded.";
    }
    else {
        echo "Sorry, there was an error uploading your file.";
    }

$stmt = $mysqli->prepare('INSERT INTO matchmaker (name, email, pictureUrl, description, age) VALUES (?, ?, ?, ?, ?)');
$stmt->bind_param($username, $email, $url, $description, $age);
$stmt->execute();
?>